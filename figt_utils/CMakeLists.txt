cmake_minimum_required (VERSION 3.13.3)
project (figt-utils)

set (CMAKE_CXX_STANDARD 17)

set (SRC)
set (INCLUDE
     include/exception-codes.hh)

set (SRC_FILES
     ${SRC}
     ${INCLUDE})

add_library (figt-utils SHARED ${SRC_FILES})
set_target_properties (figt-utils PROPERTIES LINKER_LANGUAGE CXX)

install (TARGETS figt-utils DESTINATION /usr/local/lib/libfigt)
install (FILES ${INCLUDE} DESTINATION /usr/local/include/libfigt/utils)