//
// Created by gregfmartin on 1/27/19.
//

#ifndef FIGT_ANIMATION_HH
#define FIGT_ANIMATION_HH

#include <string>
#include <vector>
#include <algorithm>
#include <iterator>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include "../../figt_utils/include/exception-codes.hh"

namespace org {
 namespace gregfmartin {
  namespace figt {
   namespace animations {
    enum class anim_loopstatus {
     LOOPING,
     NOT_LOOPING
    };

    enum class anim_progdir {
     FORWARD,
     REVERSE
    };

    class animation {
     //sf::Texture* m_texref;
     std::vector < sf::IntRect > m_frames;
     sf::Uint16 m_curframe;
     float m_framedelay;
     float m_framecount;
     anim_loopstatus m_loopstatus;
     anim_progdir m_animdir;
     sf::Uint16 m_loopframe;
     bool m_canupdate;
     std::string m_animname;
     float m_duration;
    protected:
     boost::uuids::uuid m_guid;
    public:
     animation () noexcept;

     animation (//sf::Texture* m_texref,
                const std::vector < sf::IntRect >& m_frames,
                sf::Uint16 m_curframe,
                float m_framedelay,
                float m_framecount,
                anim_loopstatus m_loopstatus,
                anim_progdir m_animdir,
                sf::Uint16 m_loopframe,
                bool m_canupdate,
                const std::string& m_animname,
                float m_duration) noexcept
      : m_frames (m_frames),
        m_curframe (m_curframe),
        m_framedelay (m_framedelay),
        m_framecount (m_framecount),
        m_loopstatus (m_loopstatus),
        m_animdir (m_animdir),
        m_loopframe (m_loopframe),
        m_canupdate (m_canupdate),
        m_animname (m_animname),
        m_duration (m_duration) {
      m_guid = boost::uuids::random_generator () ();
     }

     /**
      * @brief Obtain a pointer to the current texture reference
      * @return A pointer to the current texture reference
      * @deprecated
      */
/*     inline sf::Texture*
     get_texref () const noexcept {
      return m_texref;
     }*/

     /**
      * @brief Attempt to set the texture reference to an existing texture.
      * @param m_texref A pointer to a texture. This can be nullptr, although things may break if not careful.
      * @deprecated
      */
/*     inline void
     set_texref (sf::Texture* m_texref) noexcept {
      animation::m_texref = m_texref;
     }*/

     /**
      * @brief Obtain a reference to the current collection of animation frames.
      * @return A reference to the animation frames collection.
      */
     inline const std::vector < sf::IntRect >&
     get_frames () const noexcept {
      return m_frames;
     }

     /**
      * @brief Attempt to obtain a single animation frame from the collection based off subscript positioning.
      * @param df The desired element from the animation frame collection to obtain.
      * @return The desired element, if found.
      * @throw FIGT_EC_ANIM_FRAME404 if the desired element isn't within the animation frame collection.
      */
     inline const sf::IntRect&
     get_frame (const sf::Uint16& df) const noexcept (false) {
//      auto res = std::find (m_frames.begin (),
//                            m_frames.end (),
//                            df);
//      if (m_frames.end () == res) {
//       throw FIGT_EC_ANIM_FRAME404;
//      } else {
//       return (*res);
//      }
     }

     /**
      * @brief Replace the entire collection of animation frames.
      * @param m_frames A collection of frames to replace with the existing one.
      */
     inline void
     set_frames (const std::vector < sf::IntRect >& m_frames) noexcept {
      animation::m_frames = m_frames;
     }

     /**
      * @brief Inserts a new frame at the head of the animation frame collection.
      * @param frame A reference to an instantiated frame
      */
     inline void
     insert_frame_front (const sf::IntRect& frame) noexcept {
      m_frames.emplace (m_frames.begin (),
                        frame);
     }

     /**
      * @brief Inserts a new frame at the rear of the animation frame collection.
      * @param frame A reference to an instantiated frame.
      */
     inline void
     insert_frame_back (const sf::IntRect& frame) noexcept {
      m_frames.emplace_back (frame);
     }

     /**
      * @return The current frame of animation.
      */
     inline const sf::Uint16&
     get_curframe () const noexcept {
      return m_curframe;
     }

     /**
      * @brief Sets the current animation frame.
      * @param m_curframe A new frame for animation. No checks are made to ensure this is within acceptable boundaries.
      */
     inline void
     set_curframe (sf::Uint16 m_curframe) noexcept {
      animation::m_curframe = m_curframe;
     }

     /**
      * @brief Modifies curframe relative to current value and returns the result.
      * @param cfmod The value to adjust cuframe by, be it positive or negative.
      * @return The resultant value of curframe.
      */
     inline const sf::Uint16&
     mod_curframe (const sf::Uint16& cfmod) noexcept {
      m_curframe += cfmod;
      return m_curframe;
     }

     /**
      * @return A reference to the current value of framedelay
      */
     inline const float&
     get_framedelay () const noexcept {
      return m_framedelay;
     }

     /**
      * @brief Sets the current value of framedelay
      * @param m_framedelay The value to assign to framedelay
      */
     inline void
     set_framedelay (const float& m_framedelay) noexcept {
      animation::m_framedelay = m_framedelay;
     }

     /**
      * @brief Modifies framedelay relative to current value and returns the result.
      * @param fdmod The value to adjust framedelay by, be it positive or negative.
      * @return The resultant framedelay
      */
     inline const float&
     mod_framedelay (const float& fdmod) noexcept {
      m_framedelay += fdmod;
      return m_framedelay;
     }

     /**
      * @return A reference to the current value of framecount.
      */
     inline const float&
     get_framecount () const noexcept {
      return m_framecount;
     }

     /**
      * @brief Sets the value of framecount.
      * @param m_framecount The value to set framecount to.
      */
     inline void
     set_framecount (const float& m_framecount) noexcept {
      animation::m_framecount = m_framecount;
     }

     /**
      * @brief Modifies the current value of m_framecount.
      * @param m_mod The value to modify m_framecount by.
      */
     inline float&
     mod_framecount (const float& m_mod) noexcept {
      m_framecount += m_mod;
      return m_framecount;
     }

     /**
      * @return The current value of loopstatus.
      */
     inline const anim_loopstatus&
     get_loopstatus () const noexcept {
      return m_loopstatus;
     }

     /**
      * @brief Sets the current value of loopstatus.
      * @param m_loopstatus The value to assign to loopstatus.
      */
     inline void
     set_loopstatus (const anim_loopstatus& m_loopstatus) noexcept {
      animation::m_loopstatus = m_loopstatus;
     }

     /**
      * @return The current value of animdir.
      */
     inline const anim_progdir&
     get_animdir () const noexcept {
      return m_animdir;
     }

     /**
      * @brief Sets the current value of animdir
      * @param m_animdir The value to assign to animdir
      */
     inline void
     set_animdir (const anim_progdir& m_animdir) noexcept {
      animation::m_animdir = m_animdir;
     }

     /**
      * @return The current value of loopframe
      */
     inline sf::Uint16
     get_loopframe () const noexcept {
      return m_loopframe;
     }

     /**
      * @brief Sets the current value of loopframe
      * @param m_loopframe The value to assign to loopframe
      */
     inline void
     set_loopframe (sf::Uint16 m_loopframe) noexcept {
      animation::m_loopframe = m_loopframe;
     }

     /**
      * @brief Modifies loopframe relative to current value and returns the result.
      * @param loopframe The value to adjust loopframe by, be it positive or negative.
      * @return The resultant loopframe
      */
     inline sf::Uint16&
     mod_loopframe (const sf::Uint16& loopframe) noexcept {
      m_loopframe += loopframe;
      return m_loopframe;
     }

     /**
      * @return The current value of canupdate
      */
     inline bool
     get_canupdate () const noexcept {
      return m_canupdate;
     }

     /**
      * @brief Sets the current value of canupdate
      * @param m_canupdate The value to set canupdate to
      */
     inline void
     set_canupdate (bool m_canupdate) noexcept {
      animation::m_canupdate = m_canupdate;
     }

     /**
      * @return The current value of animname
      */
     inline const std::string&
     get_animname () const noexcept {
      return m_animname;
     }

     /**
      * @brief Set the current value of animname
      * @param m_animname The value to set animname to
      */
     inline void
     set_animname (const std::string& m_animname) noexcept {
      animation::m_animname = m_animname;
     }

     /**
      * @return The current value of duration
      */
     inline float
     get_duration () const noexcept {
      return m_duration;
     }

     /**
      * @brief Set the current value of duration
      * @param m_duration The value to set duration to
      */
     inline void
     set_duration (float m_duration) noexcept {
      animation::m_duration = m_duration;
     }

     /**
      * @return The current value of guid
      */
     inline const boost::uuids::uuid&
     get_guid () const noexcept {
      return m_guid;
     }

     /**
      * @brief Comparison operator overload to compare two animation instances by guid
      * @param a2 The opposing animation up for consideration
      * @return True if the guids are the same, false otherwise.
      */
     inline bool
     operator== (const animation& a2) const { return m_guid == a2.get_guid (); }

     /**
      * @brief Determines the number of keyframes based on duration.
      */
     void
     calculate_keyframes () noexcept;

     /**
      *
      * @param frame
      */
     void
     remove_frame (const sf::IntRect& frame) noexcept;

     void
     remove_frame (const sf::Uint16& frame) noexcept;
    };
   }
  }
 }
}

#endif //FIGT_ANIMATION_HH
