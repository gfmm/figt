//
// Created by gregfmartin on 2/6/19.
//

#ifndef FIGT_ANIMATION_REGISTRATION_HH
#define FIGT_ANIMATION_REGISTRATION_HH

#include <SFML/Config.hpp>

#include "animation.hh"

namespace org {
 namespace gregfmartin {
  namespace figt {
   namespace animations {
    struct animation_registration {
     virtual void
     register_animation (animation* anim) noexcept = 0;

     virtual void
     unregister_animation (animation* anim) noexcept = 0;

     virtual void
     unregister_animation (const sf::Uint16& anim) noexcept = 0;
    };
   }
  }
 }
}

#endif //FIGT_ANIMATION_REGISTRATION_HH
