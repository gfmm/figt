//
// Created by gregfmartin on 2/6/19.
//

#ifndef FIGT_ANIMATION_PLAYER_HH
#define FIGT_ANIMATION_PLAYER_HH

#include <vector>
#include <algorithm>
#include <iterator>

#include "animation-registration.hh"

namespace org {
 namespace gregfmartin {
  namespace figt {
   namespace animations {
    class animation_player
     : public animation_registration {
    private:
     std::vector < animation* > m_registered_animations;
    public:
     animation_player () noexcept;

     virtual~ animation_player ();

     virtual void
     register_animation (animation* anim) noexcept override;

     virtual void
     unregister_animation (animation* anim) noexcept override;

     virtual void
     unregister_animation (const sf::Uint16& anim) noexcept override;

     void
     update (const float& fpsd) noexcept;
    };
   }
  }
 }
}

#endif //FIGT_ANIMATION_PLAYER_HH
