//
// Created by gregfmartin on 2/6/19.
//

#ifndef FIGT_SPRITE_HH
#define FIGT_SPRITE_HH

#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include "animation-registration.hh"

using namespace org::gregfmartin::figt::animations;

namespace org {
 namespace gregfmartin {
  namespace figt {
   namespace entities {
    class sprite
     : public sf::Sprite,
       public animation_registration {
    private:
     std::unordered_map < std::string,
                          animation > m_animations;
     bool m_canupdate;
     float m_direction;
     sf::Vector2f m_speed;
     std::string m_curanim;
     std::vector < std::string > m_animkeys;
    protected:
     boost::uuids::uuid m_guid;
    public:
     sprite () noexcept;

     virtual~ sprite () {}

     void
     update (const float& fpsd) noexcept;

     inline const std::unordered_map < std::string,
                                       animation >&
     get_animations () const noexcept {
      return m_animations;
     }

     inline animation&
     get_animation (const std::string& animname) noexcept {
      return m_animations[animname];
     }

     inline const bool&
     get_canupdate () const {
      return m_canupdate;
     }

     inline const float&
     get_direction () const {
      return m_direction;
     }

     inline const sf::Vector2f&
     get_speed () const {
      return m_speed;
     }

     inline const std::string&
     get_curanim () const {
      return m_curanim;
     }

     inline const std::vector < std::string >&
     get_animkeys () const {
      return m_animkeys;
     }

     inline const boost::uuids::uuid&
     get_guid () const {
      return m_guid;
     }

     inline void
     set_canupdate (const bool& canupdate) noexcept {
      m_canupdate = canupdate;
     }

     inline void
     set_direction (const float& direction) noexcept {
      m_direction = direction;
     }

     inline void
     set_speed (const sf::Vector2f& speed) noexcept {
      m_speed = speed;
     }

     inline void
     set_speed (const float& xs,
                const float& ys) noexcept {
      m_speed.x = xs;
      m_speed.y = ys;
     }

     inline void
     set_speedx (const float& sx) noexcept {
      m_speed.x = sx;
     }

     inline void
     set_speedy (const float& sy) noexcept {
      m_speed.y = sy;
     }

     inline void
     set_curanim (const std::string& curanim) noexcept;

     inline const float&
     mod_direction (const float& dmod) noexcept {
      m_direction += dmod;
      return m_direction;
     }

     inline const float&
     mod_speedx (const float& xsmod) noexcept {
      m_speed.x += xsmod;
      return m_speed.x;
     }

     inline const float&
     mod_speedy (const float& ysmod) noexcept {
      m_speed.y += ysmod;
      return m_speed.y;
     }

     virtual void
     register_animation (animation* anim) noexcept override;

     virtual void
     unregister_animation (animation* anim) noexcept override;

     virtual void
     unregister_animation (const sf::Uint16& anim) noexcept override;
    };
   }
  }
 }
}

#endif //FIGT_SPRITE_HH
