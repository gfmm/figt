//
// Created by gregfmartin on 2/6/19.
//

#include "../include/sprite.hh"

org::gregfmartin::figt::entities::sprite::sprite () noexcept
 : sf::Sprite (),
   org::gregfmartin::figt::animations::animation_registration (),
   m_animations (),
   m_canupdate (false),
   m_direction (0.0f),
   m_speed (),
   m_curanim (""),
   m_animkeys (),
   m_guid (boost::uuids::random_generator () ()) {}

void
org::gregfmartin::figt::entities::sprite::update (const float& fpsd) noexcept {
 org::gregfmartin::figt::animations::animation ca = get_animation (get_curanim ());
 //setTexture ((*ca.get_texref ())); this should be set by the sprite through some other means
 setTextureRect (ca.get_frames ()[ca.get_curframe ()]);
}

void
org::gregfmartin::figt::entities::sprite::set_curanim (const std::string& curanim) noexcept {
 auto res = std::find (std::begin (m_animkeys),
                       std::end (m_animkeys),
                       curanim);
 if (std::end (m_animkeys) == res) { return; }
 else {
  m_curanim = curanim;
  for (auto i : m_animations) {
   if (m_curanim == i.first) {
    i.second
     .set_canupdate (true);
   } else {
    i.second
     .set_canupdate (false);
   }
  }
 }
}

void
org::gregfmartin::figt::entities::sprite::register_animation (animation* anim) noexcept {
 m_animations[anim->get_animname ()] = (*anim);
 m_animkeys.push_back (anim->get_animname ());
}

void
org::gregfmartin::figt::entities::sprite::unregister_animation (animation* anim) noexcept {
 auto res = m_animations.find (anim->get_animname ());
 if (m_animations.end () == res) { return; }
 else { m_animations.erase (res); }
}

void
org::gregfmartin::figt::entities::sprite::unregister_animation (const sf::Uint16& anim) noexcept {}
