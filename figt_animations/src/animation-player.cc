//
// Created by gfm on 6/22/19.
//

#include "../include/animation-player.hh"

org::gregfmartin::figt::animations::animation_player::animation_player () noexcept
 : m_registered_animations () {}

org::gregfmartin::figt::animations::animation_player::~animation_player () {}

void
org::gregfmartin::figt::animations::animation_player::register_animation (org::gregfmartin::figt::animations::animation* anim) noexcept {
 m_registered_animations.emplace_back (anim);
}

void
org::gregfmartin::figt::animations::animation_player::unregister_animation (org::gregfmartin::figt::animations::animation* anim) noexcept {
 auto itr_end = std::end (m_registered_animations);
 auto res = std::find (std::begin (m_registered_animations),
                       itr_end,
                       anim);
 if (itr_end == res) { return; }
 else { m_registered_animations.erase (res); }
}

void
org::gregfmartin::figt::animations::animation_player::unregister_animation (const sf::Uint16& anim) noexcept {
 m_registered_animations.erase (std::begin (m_registered_animations) + (anim - 1));
}

void
org::gregfmartin::figt::animations::animation_player::update (const float& fpsd) noexcept {
 for (auto& itr : m_registered_animations) {
  // check to see if we can update
  if (itr->get_canupdate ()) {
   // conditionally increment the frame counter
   if (itr->get_framecount () < itr->get_framedelay ()) {
    // increment and continue
    itr->mod_framecount (1.0f);
   } else if (itr->get_framecount () >= itr->get_framedelay ()) {
    // reset framecount
    itr->set_framecount (0.0f);

    // get the maxframe for this animation
    auto maxframe = itr->get_frames ()
                       .size ();

    // change the current frame of animation based on both the direction and looping status of the animation
    switch (itr->get_animdir ()) {
     case anim_progdir::FORWARD: {
      switch (itr->get_loopstatus ()) {
       case anim_loopstatus::LOOPING:if (itr->mod_curframe (1) >= maxframe) { itr->set_curframe (itr->get_loopframe ()); }
        break;
       case anim_loopstatus::NOT_LOOPING:if (itr->mod_curframe (1) >= maxframe) { itr->set_curframe (maxframe); }
        break;
       default:break;
      }
     }
      break;
     case anim_progdir::REVERSE: {
      switch (itr->get_loopstatus ()) {
       case anim_loopstatus::LOOPING:if (itr->mod_curframe (-1) <= 0) { itr->set_curframe (itr->get_loopframe ()); }
        break;
       case anim_loopstatus::NOT_LOOPING:if (itr->mod_curframe (-1) <= 0) { itr->set_curframe (0); }
        break;
       default: break;
      }
     }
      break;
     default:break;
    }
   }
  }
 }
}
