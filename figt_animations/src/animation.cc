//
// Created by gregfmartin on 2/6/19.
//

#include "../include/animation.hh"

void
org::gregfmartin::figt::animations::animation::calculate_keyframes () noexcept {
 // check to see if there are any frames in the container
 if (m_frames.size () <= 0) {
  m_framedelay = 1.0f;
  return;
 }

 m_framedelay = m_duration / m_frames.size ();
}

void
org::gregfmartin::figt::animations::animation::remove_frame (const sf::IntRect& frame) noexcept {
 auto res = std::find (std::begin (m_frames),
                       std::end (m_frames),
                       frame);
 if (std::end (m_frames) == res) { return; }
 else { m_frames.erase (res); }
}

void
org::gregfmartin::figt::animations::animation::remove_frame (const sf::Uint16& frame) noexcept {
 m_frames.erase (std::begin (m_frames) + (frame - 1));
}
