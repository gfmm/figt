# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/gregfmartin/CLionProjects/figt/figt_additions/icculus/physfs-2.0.3/test/test_physfs.c" "/home/gregfmartin/CLionProjects/figt/cmake-build-debug/figt_additions/icculus/physfs-2.0.3/CMakeFiles/test_physfs.dir/test/test_physfs.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "HAVE_ASSERT_H=1"
  "PHYSFS_HAVE_MNTENT_H=1"
  "PHYSFS_SUPPORTS_7Z=1"
  "PHYSFS_SUPPORTS_GRP=1"
  "PHYSFS_SUPPORTS_HOG=1"
  "PHYSFS_SUPPORTS_MVL=1"
  "PHYSFS_SUPPORTS_QPAK=1"
  "PHYSFS_SUPPORTS_WAD=1"
  "PHYSFS_SUPPORTS_ZIP=1"
  "_REENTRANT"
  "_THREAD_SAFE"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../figt_additions/icculus/physfs-2.0.3/."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/gregfmartin/CLionProjects/figt/cmake-build-debug/figt_additions/icculus/physfs-2.0.3/CMakeFiles/physfs.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
