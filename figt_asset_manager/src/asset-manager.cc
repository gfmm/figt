//
// Created by gfm on 6/22/19.
//

#include "../include/asset-manager.hh"

template < >
sf::Texture*
org::gregfmartin::figt::assets::asset_manager::get_asset (const std::string& aname) noexcept (false) {
 auto res = std::find (m_bank_textures.begin (),
                       m_bank_textures.end (),
                       aname);
 if (res == m_bank_textures.end ()) { throw FIGT_EC_AM_ASSET404; } else { return res->second; }
}

template < >
sf::Font*
org::gregfmartin::figt::assets::asset_manager::get_asset (const std::string& aname) noexcept (false) {
 auto res = std::find (m_bank_fonts.begin (),
                       m_bank_fonts.end (),
                       aname);
 if (res == m_bank_fonts.end ()) { throw FIGT_EC_AM_ASSET404; } else { return res->second; }
}

template < >
sf::SoundBuffer*
org::gregfmartin::figt::assets::asset_manager::get_asset (const std::string& aname) noexcept (false) {
 auto res = std::find (m_bank_sfx.begin (),
                       m_bank_sfx.end (),
                       aname);
 if (res == m_bank_sfx.end ()) { throw FIGT_EC_AM_ASSET404; } else { return res->second; }
}

template < >
sf::Music*
org::gregfmartin::figt::assets::asset_manager::get_asset (const std::string& aname) noexcept (false) {
 auto res = std::find (m_bank_bgm.begin (),
                       m_bank_bgm.end (),
                       aname);
 if (res == m_bank_bgm.end ()) { throw FIGT_EC_AM_ASSET404; } else { return res->second; }
}

void
org::gregfmartin::figt::assets::asset_manager::load (const std::string& arname) noexcept (false) {
 //auto b = std::bind (&org::gregfmartin::figt::assets::asset_manager::load, this, arname, std::placeholders::_1);
 auto f = std::async (std::launch::async, [&arname, this]{this->_load (arname);});
}
