//
// Created by gfm on 6/22/19.
//

#ifndef FIGT_ASSET_MANAGER_HH
#define FIGT_ASSET_MANAGER_HH

#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <iterator>
#include <thread>
#include <future>

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Music.hpp>
#include <SFML/System/NonCopyable.hpp>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <physfs.h>

#include "../../figt_utils/include/exception-codes.hh"

namespace org {
 namespace gregfmartin {
  namespace figt {
   namespace assets {
    typedef std::unordered_map < std::string,
                                 sf::Texture* > bank_textures;
    typedef std::unordered_map < std::string,
                                 sf::Font* > bank_fonts;
    typedef std::unordered_map < std::string,
                                 sf::SoundBuffer* > bank_sfx;
    typedef std::unordered_map < std::string,
                                 sf::Music* > bank_bgm;

    struct tstat_callbacks {
     virtual void
     on_start () noexcept = 0;

     virtual void
     on_update (const float& d) noexcept = 0;

     virtual void
     on_finish () noexcept = 0;
    };

    class asset_manager
     : public sf::NonCopyable {
     bank_textures m_bank_textures;
     bank_fonts m_bank_fonts;
     bank_sfx m_bank_sfx;
     bank_bgm m_bank_bgm;
     tstat_callbacks& m_loader_callbacks;

     void
     _load (const std::string& arname) noexcept;
    protected:
     boost::uuids::uuid m_guid;
    public:
     explicit asset_manager (tstat_callbacks& loader_callbacks) noexcept
      : m_bank_textures (),
        m_bank_fonts (),
        m_bank_sfx (),
        m_bank_bgm (),
        m_loader_callbacks (loader_callbacks) {
      m_guid = boost::uuids::random_generator () ();
     }

     virtual~ asset_manager () {}

     template < class T >
     T*
     get_asset (const std::string& aname) noexcept (false) {}

     template < >
     sf::Texture*
     get_asset (const std::string& aname) noexcept (false);

     template < >
     sf::Font*
     get_asset (const std::string& aname) noexcept (false);

     template < >
     sf::SoundBuffer*
     get_asset (const std::string& aname) noexcept (false);

     template < >
     sf::Music*
     get_asset (const std::string& aname) noexcept (false);

     void
     load (const std::string& arname) noexcept (false);
    };
   }
  }
 }
}

#endif //FIGT_ASSET_MANAGER_HH
